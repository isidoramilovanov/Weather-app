import City from '../entities/City';
import FiveDaysInfo from '../entities/FiveDaysInfo';

class Cities {
    CurrentWeather(cityName){
        return fetch(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&APPID=70a397b42e48b8200290d2a550289888`)
        .then(response=> {return response.json()})
        .then(data => {return new City(data)})
    }

    FiveDaysWeather(cityName){
        return fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&APPID=70a397b42e48b8200290d2a550289888`)
        .then(response=> {return response.json()})
        .then(data=> {return new FiveDaysInfo(data)})
        
    }
}

export const cities = new Cities();