
class City {
    constructor (city){
        this.id = city.id;
        this.name = city.name;
        this.weather = city.weather[0].description;
        let kToC = city.main.temp;
        this.temperature = (Math.round((kToC - 273.15 ))) ;
        this.humidity = city.main.humidity;
        this.windSpeed = city.wind.speed;
    }
}

export default City;