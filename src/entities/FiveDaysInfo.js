class FiveDaysInfo {
    constructor(fiveDaysInfo){
        this.id = fiveDaysInfo.city.id;
        this.name = fiveDaysInfo.city.name;
        this.list = fiveDaysInfo.list;
    }
}

export default FiveDaysInfo;