import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './Home';
import OneCityPage from './OneCityPage';


class App extends Component {
  render() {
    return (
      <React.Fragment>
       <Switch>
         <Route exact path='/' component={Home} />
         <Route path='/city/:name' component={OneCityPage} />
       </Switch>
  
      </React.Fragment>
    );
  }
}

export default App;
