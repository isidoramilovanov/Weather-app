import React from 'react';
import { Card, CardText, CardTitle } from 'reactstrap';
import { Link } from 'react-router-dom';


const CityItem = (props) => {
  return (
    <div>
        <Link to={`/city/${props.oneItem.name}`}>
      <div>
      
        <Card body>
          <CardTitle id="name">{props.oneItem.name}</CardTitle>
          <CardText id="weather">{props.oneItem.weather}</CardText>
          <CardText id="temp">{props.oneItem.temperature} &#8451;</CardText>
          <CardText id="humidity">Humidity: {props.oneItem.humidity}%</CardText>
          <CardText id="wind">Wind : {props.oneItem.windSpeed}km/h</CardText>
        </Card>
      
      </div>
      
      
      </Link>
    </div>
  );
};

export default CityItem;