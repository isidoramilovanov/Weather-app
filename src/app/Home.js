import React, { Component } from 'react';
import City from '../entities/City';
import {cities} from '../services/Service';
import CityItem from './CityItem';
import {Navbar} from 'reactstrap';

class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            showCity : [],
            currentCity : ''
        }
    }



handleChange=(event)=> {
    this.setState({currentCity: event.target.value});
  }

 handleSubmit=(event)=> {
    event.preventDefault();
    const cityArr = this.state.currentCity.split(',');
    cityArr.map(item => cities.CurrentWeather(item)
    .then((cities)=>{
        this.setState({
            showCity : [...this.state.showCity,cities]
        })
    }))
  }

      
render(){
    return(
        <React.Fragment>
            <div>
                <Navbar color="primary" light expand="md" id="nav-bar" className="fixed-top">
                    <form className="form-inline" onSubmit={this.handleSubmit}>
                        <input className="form-control mr-sm-2" id="textInput" type="text" value={this.state.currentCity} onChange={this.handleChange} />
                    </form>
                </Navbar>
            </div>
            <div>
                <div className="bg">
                <div className="container">
                    <div className="row">
                        <div id="showCities">
                             {this.state.showCity.map(item=> <CityItem oneItem={item} key={item.id}/>)}
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </React.Fragment>
    )
}

}

export default Home;