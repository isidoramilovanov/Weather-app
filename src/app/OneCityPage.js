import React, { Component } from 'react';
import { cities } from '../services/Service';
import OnePage from './OnePage';


class OneCityPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            oneCity : ''
        }
    }

    componentDidMount(){
        cities.FiveDaysWeather(this.props.match.params.name)
        .then((cities)=>{
            this.setState({
                oneCity : cities
            })
        })
    }

    render(){
        let content = null;
        if(this.state.oneCity !== ''){
            content = (
                <div>
                   {this.state.oneCity.list.map((item, index)=> <OnePage oneItem={item} key={index}/>)}
                </div>
            )
        }
        return(
            <div className="oneBg">
                <div className="container">
                    <div className="row ">
                        <div className="col-sm-12 col-md-12 col-lg-12 ">
                            <h1 id="text">Five days forecast for {this.state.oneCity.name}</h1>
                            {content}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default OneCityPage;