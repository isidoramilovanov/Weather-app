import React from 'react';
import { Card,CardBody, CardText, CardImg } from 'reactstrap';



const OnePage= (props) => {

    let showWaetherIcon = () => {
        if(props.oneItem.weather[0].main === "Clouds") {
         let image = "https://cdn1.iconfinder.com/data/icons/hawcons/32/698868-icon-132-cloud-128.png";
          return image;
        }
        if(props.oneItem.weather[0].main === "Rain"){
          let image = "http://icons.iconarchive.com/icons/large-icons/large-weather/256/rain-icon.png";
          return image;
        }
        if(props.oneItem.weather[0].main === "Clear"){
          let image = "http://pluspng.com/img-png/png-hot-sun-sun-icon-256.png";
          return image;
        }
        if(props.oneItem.weather[0].main === "Snow"){
          let image = "http://icons.iconarchive.com/icons/zeusbox/christmas/256/snow-flake-icon.png";
          return image;
        }
    }
    let weatherIcon = showWaetherIcon();

  return (
    <React.Fragment>
        
      <Card className="onePageCard">
        <CardBody className="onePageCardBody">
          <CardText>Date: {props.oneItem.dt_txt}</CardText>
          <CardText>Max temp: {(Math.round((props.oneItem.main.temp_max - 273.15 )))} &#8451;</CardText>
          <CardText>Min temp: {(Math.round((props.oneItem.main.temp_min - 273.15 )))} &#8451;</CardText>
          <CardText id="pressure">Pressure: {props.oneItem.main.pressure}hPa</CardText>
          <CardText id="hum">Humidity: {props.oneItem.main.humidity}%</CardText>
          <CardImg className="card-img" src={weatherIcon} />
          <CardText className="main">{props.oneItem.weather[0].main}</CardText>
          <CardText id="onePageWind">Wind: {props.oneItem.wind.speed}km/h</CardText>
        </CardBody>
      </Card>
      
    </React.Fragment>
  );
};

export default OnePage;