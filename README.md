## ** WEATHER FORECAST **

Single page application written in ReactJS, where users can search for weather forecast for all cities in the world.


### **TECHNOLOGIES**

HTML5, CSS3, JavaScript (ES6), ReactJS, reactstrap, Npm, ESLint